import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { dynamoDbClient } from "../../../dbConnection";

export const getAllTenantProperty= async(params:IControllerParams<{}>)=> {
    const properties = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :user AND contains(SK, :propertyId) AND app_status =:status",
        ExpressionAttributeValues: {
            ":user": params.user.uuid,
            ":status": "TENANTED",
            ":propertyId": "application_"
        },
    }).promise();

    return {
        message: SUCCESSFUL,
        payload: {
            count: properties.Count,
            data: properties.Items.map((each)=> {
                return {
                    leaseExpiryNotificationPeriod: each.app_lease_expiry_notification_period,
                    startDate: each.app_start_date,
                    paymentAccount: each.app_payment_account,
                    overdueRentNotifyPeriod: each.app_over_due_rent_notify_period,
                    employer: each.app_employer,
                    notes: each.app_notes,
                    firstRentPaidDue: each.app_first_rent_paid_due,
                    startAsSoonAsPossible: each.app_start_as_soon_as_possible,
                    inspectionInterval: each.app_inspection_interval,
                    propertyManager: each.app_property_manager,
                    paidInAdvance: each.app_paid_in_advance,
                    payPerMonth: each.app_pay_per_month,
                    appId: each.app_id,
                    salarySlips: each.app_salary_slips,
                    dueDateNotification: each.app_due_date_notification,
                    landlord: each.app_landlord,
                    lateRentFee: each.app_late_rent_fee,
                    securityNumber: each.app_security_number,
                    status: each.app_status,
                    uuid: each.PK,
                    totalResidents: each.app_total_residents,
                    deposit: each.app_deposit,
                    months: each.app_months,
                    years: each.app_years,
                    verifyMeBy: each.app_verifyMeBy
                }
            })
        }
    }
}