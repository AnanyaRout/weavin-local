import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";

export const contractSignTenant = async (params: IControllerParams<{}>) => {
    const application = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :user AND contains(SK, :propertyId) AND app_status= :status",
        ExpressionAttributeValues: {
            ":user": params.user.uuid,
            ":status": "APPROVED",
            ":propertyId": "application_" + params.args.params.propertyId
        },
    }).promise();

    if (application.Items.length === 0) {
        throw new HttpBadRequest("This property doesn't exist with you or owner have not approved you.");
    }

    await dynamoDbClient.update({
        TableName: "Users",
        Key: {
            PK: application.Items[0].PK,
            SK: application.Items[0].SK,
        },
        UpdateExpression: `set app_status= :app_status`,
        ExpressionAttributeValues: {
            ":app_status": "TENANTED"
        },
        ReturnValues: "UPDATED_NEW"
    }).promise();
    return {
        message: SUCCESSFUL,
        payload: {}
    }
}