import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";

export const rejectLeaseContract = async (params: IControllerParams<{}>) => {

    const application = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "contains(SK, :propertyId)",
        ExpressionAttributeValues: {
            ":propertyId": "application_" + params.args.params.propertyId
        },
    }).promise();

    if (application.Items.length === 0) {
        throw new HttpBadRequest("The owner haven't selected you for this property.");
    }
    let tenantApplication;
    const otherApplications = [];
    application.Items.map((each) => {
        if (each.PK === params.user.uuid) {
            tenantApplication = each;
            otherApplications.push({ PK: each.PK, SK: each.SK, status: "REJECTED" })
        } else {
            otherApplications.push({ PK: each.PK, SK: each.SK, status: "AWAITING" })
        }
    });
    if (!tenantApplication || tenantApplication.app_status !== "APPROVED") {
        throw new HttpBadRequest("You are not selected for this property.");
    }
    otherApplications.map(async (each) => {
        await dynamoDbClient.update({
            TableName: "Users",
            Key: {
                PK: each.PK,
                SK: each.SK,
            },
            UpdateExpression: `set app_status= :app_status`,
            ExpressionAttributeValues: {
                ":app_status": each.status
            },
            ReturnValues: "UPDATED_NEW"
        }).promise();
    })
    return {
        message: SUCCESSFUL,
        payload: {}
    }
}