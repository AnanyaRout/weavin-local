import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";
import { ICancelContractTenant } from "../../interfaces/Lease/ICancelContractTenant";

export const cancelContractTenant = async(params:IControllerParams<ICancelContractTenant>)=> {
    // const inputs= params.input;

    const application = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "contains(SK, :propertyId) AND PK=:userId AND app_status= :status",
        ExpressionAttributeValues: {
            ":status": "TENANTED",
            ":userId": params.user.uuid,
            ":propertyId": "application_" + params.args.params.propertyId
        },
    }).promise();

    if (application.Items.length === 0) {
        throw new HttpBadRequest("This property doesn't exist with you.");
    }
    return {
        message: SUCCESSFUL,
        payload: {
            application
        }
    }
}