import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";
import { IRentProperty } from "../../interfaces/Lease/IRentProperty";

export const rentProperty = async (params: IControllerParams<IRentProperty>) => {
    const inputs = params.input;
    const datass = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :user AND contains(SK, :email) OR contains(SK, :propertyId)",
        ExpressionAttributeValues: {
            ":user": inputs.propertyManager,
            ":email": "user_email",
            ":propertyId": "application_" + params.args.params.propertyId
        },
    }).promise();
    let propertyManager;
    let tenant;
    const otherApplications = [];
    datass.Items.map((each) => {
        if (each.PK === inputs.propertyManager) {
            propertyManager = each;
        }
        if (each.PK === inputs.tenant) {
            tenant = each;
        }
        if (each.PK !== inputs.tenant && each.PK !== inputs.propertyManager) {
            otherApplications.push(each);
        }
    });


    if (!propertyManager) {
        throw new HttpBadRequest("PropertyManager with this id doesn't exists.");
    }
    if (!tenant) {
        throw new HttpBadRequest("Tenant with this id doesn't exists.");
    }

    if (otherApplications.length) {
        for (let i of otherApplications) {
            await dynamoDbClient.update({
                TableName: "Users",
                Key: {
                    PK: i.PK,
                    SK: i.SK,
                },
                UpdateExpression: `set app_status= :app_status`,
                ExpressionAttributeValues: {
                    ":app_status": "DENIED"
                },
                ReturnValues: "UPDATED_NEW"
            }).promise();

        }
    }
    var param = {
        TableName: "Users",
        Key: {
            PK: tenant.PK,
            SK: tenant.SK,
        },
        UpdateExpression: `set app_years= :app_years,app_months= :app_months,
        app_start_as_soon_as_possible= :app_start_as_soon_as_possible,app_notes= :app_notes,
        app_paid_in_advance= :app_paid_in_advance,app_due_date_notification= :app_due_date_notification,
        app_deposit= :app_deposit,app_late_rent_fee=:app_late_rent_fee,app_inspection_interval=:app_inspection_interval,
        app_over_due_rent_notify_period=:app_over_due_rent_notify_period,app_payment_account=:app_payment_account,
        app_lease_expiry_notification_period=:app_lease_expiry_notification_period,app_pay_per_month=:app_pay_per_month,
        app_property_manager=:app_property_manager, app_status= :app_status,app_start_date=:app_start_date,
        app_first_rent_paid_due=:app_first_rent_paid_due`,
        ConditionExpression: "PK= :PK AND SK= :SK",
        ExpressionAttributeValues: {
            ":PK": tenant.PK,
            ":SK": tenant.SK,
            ":app_years": inputs.years,
            ":app_months": inputs.months,
            ":app_start_as_soon_as_possible": inputs.startAsSoonAsPossible,
            ":app_start_date": inputs.startAsSoonAsPossible === true ? new Date().toString() : inputs.customStartDate,
            ":app_pay_per_month": inputs.payPerMonth,
            ":app_paid_in_advance": inputs.paidInAdvance,
            ":app_first_rent_paid_due": inputs.firstRentPaidDue,
            ":app_due_date_notification": inputs.dueDateNotificationPeriod,
            ":app_deposit": inputs.deposit,
            ":app_late_rent_fee": inputs.lateRentFee,
            ":app_inspection_interval": inputs.inspectionInterval,
            ":app_over_due_rent_notify_period": inputs.overDueRentNotifyPeriod,
            ":app_payment_account": inputs.paymentAccount,
            ":app_lease_expiry_notification_period": inputs.leaseExpiryNotificationPeriod,
            ":app_notes": inputs.notes,
            ":app_property_manager": propertyManager,
            ":app_status": "APPROVED"
        },
        ReturnValues: "UPDATED_NEW"
    };
    await dynamoDbClient.update(param).promise();

    return {
        message: SUCCESSFUL,
        payload: {},
    }
}