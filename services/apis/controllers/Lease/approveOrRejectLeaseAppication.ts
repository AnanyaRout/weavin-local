import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { sendEmail } from "../../../../shared/helpers/sendEmail";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";
import { IApproveOrRejectLeaseApplication } from "../../interfaces/Lease/IApproveOrRejectLeaseApplication";

export const approveOrRejectLeaseApplication = async (params: IControllerParams<IApproveOrRejectLeaseApplication>) => {
    const status = params.input.status;
    let application;
    let tenant;
    const datas = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "(PK= :userId AND contains(SK, :propertyId)) OR (PK= :userId AND contains(SK, :emailId))",
        ExpressionAttributeValues: {
            ":userId": params.args.params.tenantId,
            ":propertyId": "application_" + params.args.params.propertyId,
            ":emailId": "user_email"
        },
    }).promise();

    datas.Items.map((each)=> {
        if(each.SK.includes("application_" + params.args.params.propertyId)) {
            application= each;
        } else if(each.SK.includes("user_email")) {
            tenant= each
        }
    })

    if (!tenant) {
        throw new HttpBadRequest("Tenant not found.");
    }
    if (!application) {
        throw new HttpBadRequest("Application not found.");
    }
    const param = {
        TableName: "Users",
        Key: {
            "PK": params.args.params.tenantId,
            "SK": application.SK,
        },
        UpdateExpression: "set app_status= :status",
        ExpressionAttributeValues: {
            ":status": status
        },
        ReturnValues: "UPDATED_NEW"
    };
    await dynamoDbClient.update(param).promise();

    if (status === "APPROVED") {
        await sendEmail(tenant.user_email, "OWNER_APPROVED", {
            body: {
              name: tenant.user_email,
              propertyId: params.args.params.propertyId
            },
            subject: {},
          });

    }
    return {
        message: SUCCESSFUL,
        payload: {application}
    }
}