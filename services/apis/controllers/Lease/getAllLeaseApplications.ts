import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";

export const getAllLeaseApplications = async (params: IControllerParams<{}>) => {
    let owner;
    let userFilterExpression = "";
    let userExpressionAttributeValue = { ":email": "user_email" };
    let c = 1;
    const filter = {
        TableName: "Users",
        FilterExpression: "begins_with(SK , :word) OR SK=:propertyId",
        ExpressionAttributeValues: {
            ":word": "application_" + params.args.params.propertyId,
            ":propertyId": params.args.params.propertyId
        },
    };

    const result = await dynamoDbClient.scan(filter).promise();
    const applications = [];
    result.Items.map((each) => {
        if (each.SK === params.args.params.propertyId) {
            owner = each;
        } else {
            applications.push(each);
        }
    });

    if (owner.PK !== params.user.uuid) {
        throw new HttpBadRequest("You can't view the applications as you are not the owner of this property.");
    }
    if (applications.length === 0) {
        throw new HttpBadRequest("No applications found.");
    }
    applications.map((each) => {
        const name = ":uuid" + c;
        if (c === applications.length) {
            userFilterExpression = userFilterExpression + "(PK = " + name + " AND contains(SK, :email))";
        } else {
            userFilterExpression = userFilterExpression + "(PK = " + name + " AND contains(SK, :email)) OR";
        }
        userExpressionAttributeValue[name] = each.PK;
        c = c + 1;
    });

    const results = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: userFilterExpression,
        ExpressionAttributeValues: userExpressionAttributeValue,
    }).promise();
    return {
        message: SUCCESSFUL,
        payload: {
            res: applications.map((each) => {
                return {
                    tenantName: results.Items.find((each1) => { return each1.PK === each.PK }).user_name,
                    tenantId: each.PK,
                    applicationId: each.SK,
                    status: each.app_status,
                    years: each.app_years,
                    months: each.app_months,
                    totalResidents: each.app_total_residents ? each.app_total_residents : null,
                    startAsSoonAsPossible: each.app_start_as_soon_as_possible ? each.app_start_as_soon_as_possible : null,
                    customStartDate: each.app_start_date ? each.app_start_date : null,
                    notes: each.app_notes ? each.app_notes : null,
                    securityNumber: each.app_security_number ? each.app_security_number : null,
                    currentLandlord: each.app_landlord ? each.app_landlord : null,
                    employer: each.app_employer ? each.app_employer : null,
                    salarySlips: each.app_salary_slips ? each.app_salary_slips : null,
                    rent: each.app_rent ? each.app_rent : null,
                    payInAdvance: each.app_payInAdvance ? each.app_payInAdvance : null,
                    dueDateNotificationPeriod: each.app_dueDateNotificationPeriod ? each.app_dueDateNotificationPeriod : null,
                    deposit: each.app_deposit ? each.app_deposit : null,
                    lateRentFee: each.app_lateRentFee ? each.app_lateRentFee : null,
                    inspectionInterval: each.app_inspectionInterval ? each.app_inspectionInterval : null,
                    overDueNotifyPeriod: each.app_overDueNotifyPeriod ? each.app_overDueNotifyPeriod : null,
                    paymentAccount: each.app_paymentAccount ? each.app_paymentAccount : null,
                    leaseExpiryNotificationPeriod: each.app_leaseExpiryNotificationPeriod ? each.app_leaseExpiryNotificationPeriod : null,
                    propertyManager: each.app_propertyManager ? each.app_propertyManager : null
                }
            })
        }
    }

}