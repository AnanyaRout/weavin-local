import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { dynamoDbClient } from "../../../dbConnection";

export const viewLeaseApplication = async (params: IControllerParams<{}>) => {
    const filter = {
        TableName: "Users",
        FilterExpression: "SK = :word",
        ExpressionAttributeValues: {
            ":word": "application_property_" + params.args.params.propertyId + "_" + params.args.params.applicationId
        },
    };

    const result = await dynamoDbClient.scan(filter).promise();

    return {
        message: SUCCESSFUL,
        payload: {
            applicationId: result.Items[0].SK,
            years: result.Items[0].app_years,
            months: result.Items[0].app_months,
            totalResidents: result.Items[0].app_total_residents,
            startAsSoonAsPossible: result.Items[0].app_start_as_soon_as_possible,
            customStartDate: result.Items[0].app_start_date,
            notes: result.Items[0].app_notes,
            securityNumber: result.Items[0].app_security_number,
            currentLandlord: result.Items[0].app_landlord,
            employer: result.Items[0].app_employer,
            salarySlips: result.Items[0].app_salary_slips
        }
    }

}