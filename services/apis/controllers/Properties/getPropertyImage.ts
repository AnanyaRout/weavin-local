import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { fileActions } from "../../../../shared/files/fileActions";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";

export const getPropertyImage = async (params: IControllerParams<{}>) => {
    const data = await fileActions("user"+params.user.uuid + "/property/" + params.args.params.propertyId+"/"+params.args.queryString.fileName,"getObject");
    return {
        message: SUCCESSFUL,
        payload: {
            data
        }
    }
}