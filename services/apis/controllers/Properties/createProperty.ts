import * as uuid from "uuid";
import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { updateSearchPropertyTable } from "../../../../shared/search/searchProperty";
import { dynamoDbClient } from "../../../dbConnection";
import { ICreateProperty } from "../../interfaces/Properties/ICreateProperties";

export const createProperty = async (params: IControllerParams<ICreateProperty>) => {

    const inputs = params.input;
    const id = uuid.v4();
    const param = {
        TableName: "Users",
        Item: {
            PK: params.user.uuid,
            SK: "property_" + id,
            prop_building_type: inputs.buildingType,
            prop_address: inputs.address,
            prop_contact: inputs.contact,
            prop_title: inputs.title,
            prop_description: inputs.description,
            prop_images: inputs.images,
            prop_size: inputs.size,
            prop_utilities: inputs.utilities,
            prop_amenities: inputs.amenities,
            prop_appliances: inputs.appliances,
            prop_comfort: inputs.comfort,
            prop_features: inputs.features,
            prop_avalability_status: "AVAILABLE",
            prop_status: "SAVED",
            prop_date: new Date().toString()
        },
    };
    await dynamoDbClient.put(param).promise();

    // await searchPropertyTableSetup(); // To create SearchProperty Table
    const data = {
        long: inputs.address.long,
        lat: inputs.address.lat,
        country: inputs.address.country,
        id
    };
    await updateSearchPropertyTable(data);
    return {
        created: true,
        message: SUCCESSFUL,
        payload: {
            uuid: "property_" + id
        }
    }
}