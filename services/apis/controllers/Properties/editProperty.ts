import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { deleteProperty, updateSearchPropertyTable } from "../../../../shared/search/searchProperty";
import { dynamoDbClient } from "../../../dbConnection";
import { ICreateProperty } from "../../interfaces/Properties/ICreateProperties";

export const editProperty = async (params: IControllerParams<ICreateProperty>) => {

    const inputs = params.input;

    var param = {
        TableName: "Users",
        Key: {
            PK: params.user.uuid,
            SK: params.args.params.propertyId,
        },
        UpdateExpression: `set prop_building_type= :prop_building_type, prop_address= :prop_address, 
        prop_contact= :prop_contact, prop_title= :prop_title ,prop_description= :prop_description,
        prop_images= :prop_images, prop_size= :prop_size, prop_utilities= :prop_utilities, 
        prop_amenities= :prop_amenities,prop_appliances= :prop_appliances, prop_comfort= :prop_comfort,
        prop_features= :prop_features, prop_avalability_status= :prop_avalability_status`,
        ConditionExpression: "PK= :PK AND SK= :SK",
        ExpressionAttributeValues: {
            ":PK": params.user.uuid,
            ":SK": params.args.params.propertyId,
            ":prop_building_type": inputs.buildingType,
            ":prop_address": inputs.address,
            ":prop_contact": inputs.contact,
            ":prop_title": inputs.title,
            ":prop_description": inputs.description,
            ":prop_images": inputs.images,
            ":prop_size": inputs.size,
            ":prop_utilities": inputs.utilities,
            ":prop_amenities": inputs.amenities,
            ":prop_appliances": inputs.appliances,
            ":prop_comfort": inputs.comfort,
            ":prop_features": inputs.features,
            ":prop_avalability_status": "AVAILABLE"
        },
        ReturnValues: "UPDATED_NEW"
    };
    await dynamoDbClient.update(param).promise();

    const data = {
        long: inputs.address.long,
        lat: inputs.address.lat,
        country: inputs.address.country,
        id: params.args.params.propertyId.split("_")[1]
    };
    await deleteProperty(data);
    await updateSearchPropertyTable(data);
    return {
        message: SUCCESSFUL,
        payload: {
            uuid: params.args.params.propertyId
        }
    }
}