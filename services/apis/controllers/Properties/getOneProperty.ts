import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";

export const getOneProperty = async (params: IControllerParams<{}>) => {
    const filter = {
        TableName: "Users",
        FilterExpression: "SK = :property",
        ExpressionAttributeValues: {
            ":property": params.args.params.propertyId
        },
    };

    const result = await dynamoDbClient.scan(filter).promise();

    if (result.Items[0].status === "SAVED" || result.Items[0].PK !== params.user.uuid) {
        throw new HttpBadRequest("Sorry, You are not authorized to view this property.");
    }
    return {
        message: SUCCESSFUL,
        payload: {
            propertyId: result.Items[0].SK,
            public: {
                status: result.Items[0].prop_status ? result.Items[0].prop_status : null,
                availableSince: result.Items[0].prop_available_since ? result.Items[0].prop_available_since : null,
                propertyManager: result.Items[0].prop_property_manager ? result.Items[0].prop_property_manager : null,
                requirements: result.Items[0].prop_requirements ? result.Items[0].prop_requirements : null,
                buildingType: result.Items[0].prop_building_type ? result.Items[0].prop_building_type : null,
                address: result.Items[0].prop_address,
                contact: result.Items[0].prop_contact ? result.Items[0].prop_contact : null,
                title: result.Items[0].prop_title ? result.Items[0].prop_title : null,
                description: result.Items[0].prop_description ? result.Items[0].prop_description : null,
                images: result.Items[0].prop_images ? result.Items[0].prop_images : null,
                size: result.Items[0].prop_size ? result.Items[0].prop_size : null,
                utilities: result.Items[0].prop_utilities ? result.Items[0].prop_utilities : null,
                amenities: result.Items[0].prop_amenities ? result.Items[0].prop_amenities : null,
                appliances: result.Items[0].prop_appliances ? result.Items[0].prop_appliances : null,
                comfort: result.Items[0].prop_comfort ? result.Items[0].prop_comfort : null,
                features: result.Items[0].prop_features ? result.Items[0].prop_features : null
            },
            warranty: null,
            lease: null,
            incidents: null,
            payments: null
        }
    }
}