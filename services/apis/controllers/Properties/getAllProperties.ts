import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";

export const getAllProperties = async (params: IControllerParams<{}>) => {
    if (!params.args.queryString.screen && params.args.queryString.screen !== "OWNER" && params.args.queryString.screen !== "TENANT") {
        throw new HttpBadRequest("Please enter in which screen you are in. OWNER or TENANT.");
    }
    const filter = {
        TableName: "Users",
        FilterExpression: "begins_with(SK , :word)",
        ExpressionAttributeValues: {
            ":word": "property_",
        },
    };

    if (params.args.queryString.screen === "OWNER") {
        filter.FilterExpression = filter.FilterExpression + " AND PK= :userId";
        filter.ExpressionAttributeValues[":userId"] = params.user.uuid;
        if (params.args.queryString.status) {
            filter.FilterExpression = filter.FilterExpression + " AND prop_status= :status";
            filter.ExpressionAttributeValues[":status"] = params.args.queryString.status;
        }
    } else {
        filter.FilterExpression = filter.FilterExpression + " AND prop_status= :status";
        filter.ExpressionAttributeValues[":status"] = "PUBLISHED";
    }
    const result = await dynamoDbClient.scan(filter).promise();

    return {
        message: SUCCESSFUL,
        payload: result.Items.map((each) => {
            return {
                propertyId: each.SK,
                public: {
                    status: each.prop_status,
                    availableSince: each.prop_available_since ? each.prop_available_since : null,
                    propertyManager: each.prop_property_manager ? each.prop_property_manager : null,
                    requirements: each.prop_requirements ? each.prop_requirements : null,
                    buildingType: each.prop_building_type ? each.prop_building_type : null,
                    address: each.prop_address,
                    contact: each.prop_contact ? each.prop_contact : null,
                    title: each.prop_title ? each.prop_title : null,
                    description: each.prop_description ? each.prop_description : null,
                    images: each.prop_images ? each.prop_images : null,
                    size: each.prop_size ? each.prop_size : null,
                    utilities: each.prop_utilities ? each.prop_utilities : null,
                    amenities: each.prop_amenities ? each.prop_amenities : null,
                    appliances: each.prop_appliances ? each.prop_appliances : null,
                    comfort: each.prop_comfort ? each.prop_comfort : null,
                    features: each.prop_features ? each.prop_features : null
                },
                warranty: null,
                lease: null,
                incidents: null,
                payments: null
            }
        })
    }
}
