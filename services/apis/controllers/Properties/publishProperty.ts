import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";
import { IPublishProperty } from "../../interfaces/Properties/IPublishProperty";

export const publishProperty = async(params:IControllerParams<IPublishProperty>) => {
    const inputs= params.input;
    const data = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :user AND contains(SK, :email)",
        ExpressionAttributeValues: {
            ":user": inputs.propertyManager,
            ":email": "user_email"
        },
    }).promise();
    if (data.Items.length === 0) {
        throw new HttpBadRequest("PropertyManager with this id doesn't exists.");
    }
    const property = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :user AND SK =:propertyId",
        ExpressionAttributeValues: {
            ":user": params.user.uuid,
            ":propertyId": params.args.params.propertyId
        },
    }).promise();
    if (property.Items.length === 0) {
        throw new HttpBadRequest("You are not authorized to publish it.");
    }
    var param = {
        TableName:"Users",
        Key:{
            PK: params.user.uuid,
            SK: params.args.params.propertyId,
        },
        UpdateExpression: "set prop_available_since= :prop_available_since, prop_rent_per_month= :prop_rent_per_month, prop_deposite= :prop_deposite, prop_application_fee= :prop_application_fee ,prop_property_manager= :prop_property_manager,prop_requirements= :prop_requirements,prop_status= :prop_status",
        ConditionExpression: "PK= :PK AND SK= :SK",
        ExpressionAttributeValues:{
            ":PK": params.user.uuid,
            ":SK": params.args.params.propertyId,
            ":prop_available_since": inputs.availableSince,
            ":prop_rent_per_month": inputs.rentPerMonth,
            ":prop_deposite": inputs.deposite,
            ":prop_application_fee": inputs.applicationFee,
            ":prop_property_manager": data.Items[0],
            ":prop_requirements": inputs.requirements,
            ":prop_status": inputs.status
        },
        ReturnValues:"UPDATED_NEW"
    };
    await dynamoDbClient.update(param).promise();

    return {
        message: SUCCESSFUL,
        payload: {
            propertyId: params.args.params.propertyId
        }
    }
}