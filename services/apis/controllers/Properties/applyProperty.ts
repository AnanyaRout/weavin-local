import * as uuid from "uuid";
import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../../shared/lib/exceptions/HttpBadRequest";
import { dynamoDbClient } from "../../../dbConnection";
import { IApplyProperty } from "../../interfaces/Properties/IApplyProperty";

export const applyProperty = async (params: IControllerParams<IApplyProperty>) => {
    const inputs = params.input;
    let property;
    let application;
    let ownProperty;
    const id = uuid.v4();
    const userId= params.user.uuid;
    const filter = {
        TableName: "Users",
        FilterExpression: "contains(SK , :propertyId)",
        ExpressionAttributeValues: {
            ":propertyId": params.args.params.propertyId,
            // ":userId": userId
        },
    };

    const result = await dynamoDbClient.scan(filter).promise();
    


    result.Items.map((each)=> {
        if(each.SK=== params.args.params.propertyId){
            property= each;
        }
        if(each.PK=== userId && each.SK.includes("application_"+params.args.params.propertyId)===true){
            application= each;
        }
        if(each.PK=== userId && each.SK===params.args.params.propertyId){
            ownProperty= each;
        }
    });

    if(ownProperty) {
        throw new HttpBadRequest("You can't apply for this property as you are the owner.");
    }
    if(!property) {
        throw new HttpBadRequest("Property with this uuid doesn't exist.");
    }
    if(property.prop_status !== "PUBLISHED" ) {
        throw new HttpBadRequest("Sorry, You can't apply for this property as this is not published yet.");
    }
    if(application) {
        throw new HttpBadRequest("You already have applied for this property.");
    }
    const param = {
        TableName: "Users",
        Item: {
            PK: userId,
            SK: "application_" + params.args.params.propertyId + "_" + id,
            app_id: id,
            app_years: inputs.years,
            app_months: inputs.months,
            app_total_residents: inputs.totalResidents,
            app_start_as_soon_as_possible: inputs.startAsSoonAsPossible,
            app_start_date: inputs.startAsSoonAsPossible=== true ? new Date().toString() : inputs.customStartDate,
            app_notes: inputs.notes,
            app_verifyMeBy: inputs.verifyMeBy,
            app_security_number: inputs.securityNumber,
            app_landlord: inputs.currentLandlord,
            app_employer: inputs.employer,
            app_salary_slips: inputs.salarySlips,
            app_status: "AWAITING"
        },
    };
    await dynamoDbClient.put(param).promise();

    return {
        message: SUCCESSFUL,
        payload: {}
    }
}