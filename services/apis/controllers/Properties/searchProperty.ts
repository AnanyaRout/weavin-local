import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { HttpNoContent } from "../../../../shared/lib/exceptions/HttpNoContent";
import { searchProperty } from "../../../../shared/search/searchProperty";
import { dynamoDbClient } from "../../../dbConnection";

export const searchProperties = async (params: IControllerParams<{}>) => {
    const expressionAttributeValues = {};
    let filterExpression = "";
    const inputs = params.args.queryString;
    const data = {
        radius: inputs.radius,
        lat: parseInt(inputs.lat),
        long: parseInt(inputs.long)
    }
    const res = await searchProperty(data);

    if (res.length === 0) {
        throw new HttpNoContent("NO DATA FOUND");
    }
    const propertyIds = res.map((each) => {
        return each.propertyId.S
    });


    let index = 0;

    propertyIds.forEach((value) => {
        index++;
        var propertyKey = ":propertyvalue" + index;
        expressionAttributeValues[propertyKey.toString()] = value;
    });
    filterExpression = filterExpression + "SK IN (" + Object.keys(expressionAttributeValues).toString() + ") AND prop_status = :status";
    expressionAttributeValues[":status"] = "PUBLISHED";


    if (params.args.queryString.buildingType) {
        filterExpression = filterExpression + " AND prop_building_type = :buildingType";
        expressionAttributeValues[":buildingType"] = params.args.queryString.buildingType;
    }
    if (params.args.queryString.location) {
        filterExpression = filterExpression + " AND contains(prop_address.locality , :location)";
        expressionAttributeValues[":location"] = params.args.queryString.location;
    }
    if (params.args.queryString.minimumPrice) {
        filterExpression = filterExpression + " AND prop_application_fee >= :minimumPrice";
        expressionAttributeValues[":minimumPrice"] = parseFloat(params.args.queryString.minimumPrice);
    }
    if (params.args.queryString.maximumPrice) {
        filterExpression = filterExpression + " AND prop_application_fee <= :maximumPrice";
        expressionAttributeValues[":maximumPrice"] = parseFloat(params.args.queryString.maximumPrice);
    }
    if (params.args.queryString.minimumSize) {
        filterExpression = filterExpression + " AND prop_size.surface >= :minimumSize";
        expressionAttributeValues[":minimumSize"] = parseFloat(params.args.queryString.minimumSize);
    }
    if (params.args.queryString.maximumSize) {
        filterExpression = filterExpression + " AND prop_size.surface <= :maximumSize";
        expressionAttributeValues[":maximumSize"] = parseFloat(params.args.queryString.maximumSize);
    }
    if (params.args.queryString.bedRooms) {
        filterExpression = filterExpression + " AND prop_size.bedRooms = :bedRooms";
        expressionAttributeValues[":bedRooms"] = parseFloat(params.args.queryString.bedRooms);
    }
    if (params.args.queryString.waterUtility) {
        filterExpression = filterExpression + " AND contains(prop_utilities , :waterUtility)";
        expressionAttributeValues[":waterUtility"] = {
            name: "WATER",
            selected: params.args.queryString.waterUtility === "true" ? true : false
        };
    }
    if (params.args.queryString.electricity) {
        filterExpression = filterExpression + " AND contains(prop_utilities , :electricity)";
        expressionAttributeValues[":electricity"] = {
            name: "ELECTRICITY",
            selected: params.args.queryString.electricity === "true" ? true : false
        };
    }
    if (params.args.queryString.heating) {
        filterExpression = filterExpression + " AND contains(prop_utilities , :heating)";
        expressionAttributeValues[":heating"] = {
            name: "HEATING",
            selected: params.args.queryString.heating === "true" ? true : false
        };
    }
    if (params.args.queryString.climatization) {
        filterExpression = filterExpression + " AND contains(prop_utilities , :climatization)";
        expressionAttributeValues[":climatization"] = {
            name: "CLIMATIZATION",
            selected: params.args.queryString.climatization === "true" ? true : false
        };
    }
    if (params.args.queryString.petFriendly) {
        filterExpression = filterExpression + " AND contains(prop_amenities , :petFriendly)";
        expressionAttributeValues[":petFriendly"] = {
            name: "PET_FRIENDLY",
            selected: params.args.queryString.petFriendly === "true" ? true : false
        };
    }
    if (params.args.queryString.kidsFriendly) {
        filterExpression = filterExpression + " AND contains(prop_amenities , :kidsFriendly)";
        expressionAttributeValues[":kidsFriendly"] = {
            name: "KIDS_FRIENDLY",
            selected: params.args.queryString.kidsFriendly === "true" ? true : false
        };
    }
    if (params.args.queryString.smokingAllowed) {
        filterExpression = filterExpression + " AND contains(prop_amenities , :smokingAllowed)";
        expressionAttributeValues[":smokingAllowed"] = {
            name: "SMOKING_ALLOWED",
            selected: params.args.queryString.smokingAllowed === "true" ? true : false
        };
    }
    if (params.args.queryString.wifi) {
        filterExpression = filterExpression + " AND contains(prop_amenities , :wifi)";
        expressionAttributeValues[":wifi"] = {
            name: "WIFI",
            selected: params.args.queryString.smokingAllowed === "true" ? true : false
        };
    }
    if (params.args.queryString.storeys) {
        filterExpression = filterExpression + " AND contains(prop_amenities , :storeys)";
        expressionAttributeValues[":storeys"] = {
            name: "STOREYS",
            selected: params.args.queryString.smokingAllowed === "true" ? true : false
        };
    }
    if (params.args.queryString.fridge) {
        filterExpression = filterExpression + " AND contains(prop_appliances , :fridge)";
        expressionAttributeValues[":fridge"] = {
            name: "FRIDGE",
            selected: params.args.queryString.fridge === "true" ? true : false
        };
    }
    if (params.args.queryString.washingMachine) {
        filterExpression = filterExpression + " AND contains(prop_appliances , :washingMachine)";
        expressionAttributeValues[":washingMachine"] = {
            name: "WASHING_MACHINE",
            selected: params.args.queryString.washingMachine === "true" ? true : false
        };
    }
    if (params.args.queryString.coffeeMachine) {
        filterExpression = filterExpression + " AND contains(prop_appliances , :coffeeMachine)";
        expressionAttributeValues[":coffeeMachine"] = {
            name: "COFFEE_MACHINE",
            selected: params.args.queryString.coffeeMachine === "true" ? true : false
        };
    }
    if (params.args.queryString.TV) {
        filterExpression = filterExpression + " AND contains(prop_appliances , :TV)";
        expressionAttributeValues[":TV"] = {
            name: "TV",
            selected: params.args.queryString.TV === "true" ? true : false
        };
    }
    // if (params.args.queryString.lift) {
    //     filterExpression = filterExpression + " AND prop_comfort contains(others , :lift)";
    //     expressionAttributeValues[":lift"] = {
    //         name: "LIFT",
    //         selected: params.args.queryString.lift === "true" ? true : false
    //     };
    // }
    // if(params.args.queryString.terrace) {
    //     filterExpression= filterExpression+ " AND contains(prop_comfort.others , :terrace)";
    //     expressionAttributeValues[":terrace"]={
    //         name: "TERRACE",
    //         selected: params.args.queryString.terrace==="true"? true:false
    //     };
    // }
    // if (params.args.queryString.balcony) {
    //     filterExpression = filterExpression + " AND contains(prop_comfort.others , :balcony)";
    //     expressionAttributeValues[":balcony"] = {
    //         name: "BALCONY",
    //         selected: params.args.queryString.balcony === "true" ? true : false
    //     };
    // }
    if (params.args.queryString.parking) {
        filterExpression = filterExpression + " AND contains(prop_features , :parking)";
        expressionAttributeValues[":parking"] = {
            name: "PARKING",
            selected: params.args.queryString.parking === "true" ? true : false
        };
    }
    if (params.args.queryString.minimumFloor) {
        filterExpression = filterExpression + " AND prop_comfort.floorLevel >= :minimumFloor";
        expressionAttributeValues[":minimumFloor"] = parseFloat(params.args.queryString.minimumFloor);
    }
    if (params.args.queryString.maximumFloor) {
        filterExpression = filterExpression + " AND prop_comfort.floorLevel <= :maximumFloor";
        expressionAttributeValues[":maximumFloor"] = parseFloat(params.args.queryString.maximumFloor);
    }
    const param = {
        TableName: "Users",
        FilterExpression: filterExpression,
        ExpressionAttributeValues: expressionAttributeValues,
    };

    const result = await dynamoDbClient.scan(param).promise();
    return {
        message: SUCCESSFUL,
        payload: result.Items.map((each) => {
            return {
                propertyId: each.SK,
                public: {
                    status: each.prop_status,
                    availableSince: each.prop_available_since,
                    propertyManager: each.prop_property_manager,
                    requirements: each.prop_requirements,
                    buildingType: each.prop_building_type,
                    address: each.prop_address,
                    contact: each.prop_contact,
                    title: each.prop_title,
                    description: each.prop_description,
                    images: each.prop_images,
                    size: each.prop_size,
                    utilities: each.prop_utilities,
                    amenities: each.prop_amenities,
                    appliances: each.prop_appliances,
                    comfort: each.prop_comfort,
                    features: each.prop_features
                }
            }
        })
    }
}