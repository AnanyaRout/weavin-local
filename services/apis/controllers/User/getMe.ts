import { SUCCESSFUL } from "../../../../shared/constants/httpSuccessMessages";
import { IControllerParams } from "../../../../shared/interfaces/IControllerParams";
import { dynamoDbClient } from "../../../dbConnection";

export const getMe= async(params: IControllerParams<{}>) => {

    const data = await dynamoDbClient.scan({
        TableName: "Users",
        FilterExpression: "PK = :uuid AND contains(SK, :email)",
        ExpressionAttributeValues: {
            ":uuid": params.user.uuid,
            ":email": "user_email"
        },
    }).promise();

    return {
        message: SUCCESSFUL,
        payload: {
            uuid: data.Items[0].PK,
            user_email: data.Items[0].user_email,
            user_phone: data.Items[0].user_phone,
            user_name: data.Items[0].user_name,
            user_created_date: data.Items[0].user_created_date
        }
    }
}