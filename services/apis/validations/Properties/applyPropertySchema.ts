import { checkSchema } from "express-validator";
export const applyPropertySchema = checkSchema({
    "years": {
        exists: {
            errorMessage: "Year is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Year is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Year should be an integer.",
        },
    },
    "months": {
        exists: {
            errorMessage: "Month is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Month is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Month should be an integer.",
        },
    },
    "totalResidents": {
        exists: {
            errorMessage: "Total Residents is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Total Residents is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Total Residents should be an integer.",
        },
    },
    "startAsSoonAsPossible": {
        exists: {
            errorMessage: "StartAsSoonAsPossible is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "StartAsSoonAsPossible is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "StartAsSoonAsPossible should be a boolean.",
        },
    },
    "customStartDate": {
        exists: {
            errorMessage: "CustomStartDate is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CustomStartDate is required.",
            negated: true,
        },
        isDate: {
            errorMessage: "CustomStartDate should be a date.",
        },
    },
    "notes": {
        exists: {
            errorMessage: "Notes is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Notes is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Notes should be a string.",
        },
    },
    "verifyMeBy": {
        exists: {
            errorMessage: "VerifyMeBy is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "VerifyMeBy is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "VerifyMeBy should be a boolean.",
        },
    },
    "securityNumber": {
        exists: {
            errorMessage: "SecurityNumber is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SecurityNumber is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "SecurityNumber should be an object.",
        },
    },
    "securityNumber.documentType": {
        exists: {
            errorMessage: "SecurityNumber documentType is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SecurityNumber documentType is required.",
            negated: true,
        },
        isString: {
            errorMessage: "SecurityNumber documentType should be a string.",
        },
    },
    "securityNumber.number": {
        exists: {
            errorMessage: "SecurityNumber number is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SecurityNumber number is required.",
            negated: true,
        },
        isString: {
            errorMessage: "SecurityNumber number should be a string.",
        },
    },
    "currentLandlord": {
        exists: {
            errorMessage: "CurrentLandlord is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "CurrentLandlord should be an object.",
        },
    },
    "currentLandlord.propertyTenantedEarlier": {
        exists: {
            errorMessage: "PropertyTenantedEarlier is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PropertyTenantedEarlier is required.",
            negated: true,
        },
        isString: {
            errorMessage: "PropertyTenantedEarlier should be a string.",
        },
    },
    "currentLandlord.name": {
        exists: {
            errorMessage: "CurrentLandlord name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord name is required.",
            negated: true,
        },
        isString: {
            errorMessage: "CurrentLandlord name should be a string.",
        },
    },
    "currentLandlord.address": {
        exists: {
            errorMessage: "CurrentLandlord address is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord address is required.",
            negated: true,
        },
        isString: {
            errorMessage: "CurrentLandlord address should be a string.",
        },
    },
    "currentLandlord.phone": {
        exists: {
            errorMessage: "CurrentLandlord phone is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord phone is required.",
            negated: true,
        },
        isString: {
            errorMessage: "CurrentLandlord phone should be a string.",
        },
    },
    "currentLandlord.ownerEmail": {
        exists: {
            errorMessage: "CurrentLandlord ownerEmail is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord ownerEmail is required.",
            negated: true,
        },
        isString: {
            errorMessage: "CurrentLandlord ownerEmail should be a string.",
        },
        isEmail: {
            errorMessage: "Please enter a valid email.",
        },
    },
    "currentLandlord.monthlyRent": {
        exists: {
            errorMessage: "CurrentLandlord monthlyRent is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CurrentLandlord monthlyRent is required.",
            negated: true,
        },
        isString: {
            errorMessage: "CurrentLandlord monthlyRent should be a string.",
        },
    },
    "currentLandlord.tenantEmail": {
        exists: {
            errorMessage: "TenantEmail is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "TenantEmail is required.",
            negated: true,
        },
        isString: {
            errorMessage: "TenantEmail should be a string.",
        },
        isEmail: {
            errorMessage: "Please enter a valid email.",
        },
    },
    "employer": {
        exists: {
            errorMessage: "Employer is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "Employer should be an object.",
        },
    },
    "employer.employerId": {
        exists: {
            errorMessage: "Employer Id is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer Id is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer Id should be a string.",
        },
    },
    "employer.companyName": {
        exists: {
            errorMessage: "Employer companyName is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer companyName is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer companyName should be a string.",
        },
    },
    "employer.companyAddress": {
        exists: {
            errorMessage: "Employer companyAddress is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer companyAddress is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer companyAddress should be a string.",
        },
    },
    "employer.supervisorName": {
        exists: {
            errorMessage: "Employer supervisorName is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer supervisorName is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer supervisorName should be a string.",
        },
    },
    "employer.supervisorEmail": {
        exists: {
            errorMessage: "Employer supervisorEmail is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer supervisorEmail is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer supervisorEmail should be a string.",
        },
        isEmail: {
            errorMessage: "Please enter a valid email.",
        }
    },
    "employer.monthlyIncome": {
        exists: {
            errorMessage: "Employer monthlyIncome is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Employer monthlyIncome is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Employer monthlyIncome should be a string.",
        },
    },
    "salarySlips": {
        exists: {
            errorMessage: "SalarySlips is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SalarySlips is required.",
            negated: true,
        },
    },
    "salarySlips.salarySlipOfPreviousMonth": {
        exists: {
            errorMessage: "SalarySlips salarySlipOfPreviousMonth is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SalarySlips salarySlipOfPreviousMonth is required.",
            negated: true,
        },
        isString: {
            errorMessage: "SalarySlips salarySlipOfPreviousMonth should be a string.",
        },
    },
    "salarySlips.salarySlipOfLatestMonth": {
        exists: {
            errorMessage: "SalarySlips salarySlipOfLatestMonth is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "SalarySlips salarySlipOfLatestMonth is required.",
            negated: true,
        },
        isString: {
            errorMessage: "SalarySlips salarySlipOfLatestMonth should be a string.",
        },
    }
})