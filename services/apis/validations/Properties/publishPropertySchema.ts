import { checkSchema } from "express-validator";
export const publishPropertySchema = checkSchema({
    "status": {
        exists: {
            errorMessage: "Status is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Status is required.",
            negated: true,
        },
        isIn: {
            options: [["SAVED", "PUBLISHED"]],
            errorMessage: "Invalid Name."
        }
    },
    "availableSince": {
        exists: {
            errorMessage: "AvailableSince is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "AvailableSince is required.",
            negated: true,
        },
        isString: {
            errorMessage: "AvailableSince should be a string.",
        },
        optional: true
    },
    "rentPerMonth": {
        exists: {
            errorMessage: "RentPerMonth is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "RentPerMonth is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "RentPerMonth should be a number.",
        },
        optional: true
    },
    "deposite": {
        exists: {
            errorMessage: "Deposite is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Deposite is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Deposite should be a number.",
        },
        optional: true
    },
    "applicationFee": {
        exists: {
            errorMessage: "ApplicationFee is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "ApplicationFee is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "ApplicationFee should be a number.",
        },
        optional: true
    },
    "propertyManager": {
        exists: {
            errorMessage: "PropertyManager is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PropertyManager is required.",
            negated: true,
        },
        isString: {
            errorMessage: "PropertyManager should be a string.",
        },
        optional: true
    },
    "requirements": {
        exists: {
            errorMessage: "Requirements is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Requirements is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "Requirements should be an object.",
        },
        optional: true
    },
    "requirements.showMonthlyIncome": {
        exists: {
            errorMessage: "ShowMonthlyIncome is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "ShowMonthlyIncome is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "ShowMonthlyIncome must be a boolean.",
        },
        optional: true
    },
    "requirements.showDocumentWIthPicture": {
        exists: {
            errorMessage: "ShowDocumentWIthPicture is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "ShowDocumentWIthPicture is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "ShowDocumentWIthPicture must be a boolean.",
        },
        optional: true
    },
    "requirements.showIncomeProof": {
        exists: {
            errorMessage: "ShowIncomeProof is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "ShowIncomeProof is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "ShowIncomeProof must be a boolean.",
        },
        optional: true
    },
    "requirements.fixInsurance": {
        exists: {
            errorMessage: "FixInsurance is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "FixInsurance is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "FixInsurance must be a boolean.",
        },
        optional: true
    }
});

