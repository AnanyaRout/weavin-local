import { checkSchema } from "express-validator";
export const createPropertySchema = checkSchema({
    "buildingType": {
        exists: {
            errorMessage: "BuildingType is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "BuildingType is required.",
            negated: true,
        },
        isIn: {
            options: [["APPARTMENT", "STUDIO_APPARTMENT", "VILLA", "FAMILY_HOUSE", "SHARED_APPARTMENT"]],
            errorMessage: "Invalid BuildingType"
        },
        optional: true
    },
    "address": {
        exists: {
            errorMessage: "Address is missing.",
        },
        in: ["body"],
        isObject: {
            errorMessage: "Address should be an object.",
        },
        isEmpty: {
            errorMessage: "Address is required.",
            negated: true,
        },
    },
    "address.houseNumber": {
        exists: {
            errorMessage: "HouseNumber is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "HouseNumber is required.",
            negated: true,
        },
        isString: {
            errorMessage: "HouseNumber should be a string.",
        },
    },
    "address.street": {
        exists: {
            errorMessage: "Street is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Street is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Street should be a string.",
        },
    },
    "address.locality": {
        exists: {
            errorMessage: "Locality is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Locality is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Locality should be a string.",
        },
    },
    "address.country": {
        exists: {
            errorMessage: "Country is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Country is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Country should be a string.",
        },
    },
    "address.long": {
        exists: {
            errorMessage: "Longitude is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Longitude is required.",
            negated: true,
        },
        isFloat: {
            errorMessage: "Longitude should be a number.",
        },
    },
    "address.lat": {
        exists: {
            errorMessage: "Latitude is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Latitude is required.",
            negated: true,
        },
        isFloat: {
            errorMessage: "Longitude should be a number.",
        },
    },
    "contact": {
        exists: {
            errorMessage: "Contact is missing.",
        },
        in: ["body"],
        isObject: {
            errorMessage: "Contact should be an object.",
        },
        isEmpty: {
            errorMessage: "Contact is required.",
            negated: true,
        },
        optional: true
    },
    "contact.code": {
        exists: {
            errorMessage: "Contact code is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Contact code is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Contact code should be a string.",
        },
        optional: true
    },
    "contact.phone": {
        isLength: {
            errorMessage: "The phone number should 10-12 characters long",
            options: { min: 10, max: 12 },
        },
        exists: {
            errorMessage: "Contact phone is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Contact phone is required.",
            negated: true,
        },
        isNumeric: {
            errorMessage: "Contact phone should be a number.",
        },
        optional: true
    },
    "title": {
        isLength: {
            errorMessage: "The title should maximum 60 characters long",
            options: { min: 0, max: 60 },
        },
        exists: {
            errorMessage: "Title is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Title is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Title should be a string.",
        },
        optional: true
    },
    "description": {
        custom: {
            options: ((value) => {
                const wordLength = value.replace(/\s+/g, " ").trim().split(' ').length;
                if (wordLength > 1000) {
                    return Promise.reject(new Error("Should not exceed 1000 words."));
                }
                return true;
            }),
        },
        exists: {
            errorMessage: "Description is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Description is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Description should be a string.",
        },
        optional: true
    },
    "images": {
        custom: {
            options: ((value) => {
                let mainImages = 0;
                value.map((each) => { if (each.mainImage === true) { mainImages = mainImages + 1; } });
                if (mainImages === 0) {
                    return Promise.reject(new Error("Should have atleast 1 mainImage."));
                } else if (mainImages > 1) {
                    return Promise.reject(new Error("More than 1 mainImage is not allowed."));
                }
                return true;
            }),
        },
        exists: {
            errorMessage: "Images are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Images must be an array",
        },
        isEmpty: {
            errorMessage: "Images are required",
            negated: true,
        },
        optional: true
    },
    "images.*.link": {
        exists: {
            errorMessage: "Image Links are missing",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Image Links are required",
            negated: true,
        },
        isString: {
            errorMessage: "Image should be a string.",
        },
        optional: true
    },
    "images.*.mainImage": {
        exists: {
            errorMessage: "Main Image are missing",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Main Image are required",
            negated: true,
        },
        optional: true
    },
    "size": {
        exists: {
            errorMessage: "Size is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Size is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "Size should be an object.",
        },
        optional: true
    },
    "size.surface": {
        exists: {
            errorMessage: "Surface is missing.",
        },
        in: ["body"],
        isInt: {
            errorMessage: "Surface should be a number.",
        },
        isEmpty: {
            errorMessage: "Surface is required.",
            negated: true,
        },
        optional: true
    },
    "size.bedRooms": {
        exists: {
            errorMessage: "BedRooms is missing.",
        },
        in: ["body"],
        isInt: {
            errorMessage: "BedRooms should be a number.",
        },
        isEmpty: {
            errorMessage: "BedRooms is required.",
            negated: true,
        },
        optional: true
    },
    "size.bathRooms": {
        exists: {
            errorMessage: "BathRooms is missing.",
        },
        in: ["body"],
        isInt: {
            errorMessage: "Bathrooms should be a number.",
        },
        isEmpty: {
            errorMessage: "BathRooms is required.",
            negated: true,
        },
        optional: true
    },
    "utilities": {
        exists: {
            errorMessage: "Utilities are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Utilities must be an array",
        },
        isEmpty: {
            errorMessage: "Utilities are required",
            negated: true,
        },
        optional: true
    },
    "utilities.*.name": {
        exists: {
            errorMessage: "Name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Name is required.",
            negated: true,
        },
        isIn: {
            options: [["WATER", "ELECTRICITY", "HEATING", "CLIMATIZATION"]],
            errorMessage: "Invalid Name."
        },
        optional: true
    },
    "utilities.*.selected": {
        exists: {
            errorMessage: "Selected is missing.",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Selected is required.",
            negated: true,
        },
        optional: true
    },
    "amenities": {
        exists: {
            errorMessage: "Amenities are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Amenities must be an array.",
        },
        isEmpty: {
            errorMessage: "Amenities are required",
            negated: true,
        },
        optional: true
    },
    "amenities.*.name": {
        exists: {
            errorMessage: "Name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Name is required.",
            negated: true,
        },
        isIn: {
            options: [["PETS_FRIENDLY", "KIDS_FRIENDLY", "SMOKING_ALLOWED", "WIFI", "STOREYS"]],
            errorMessage: "Invalid Name."
        },
        optional: true
    },
    "amenities.*.selected": {
        exists: {
            errorMessage: "Selected is missing.",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Selected is required.",
            negated: true,
        },
        optional: true
    },
    "appliances": {
        exists: {
            errorMessage: "Appliances are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Appliances must be an array.",
        },
        isEmpty: {
            errorMessage: "Appliances are required",
            negated: true,
        },
        optional: true
    },
    "appliances.*.name": {
        exists: {
            errorMessage: "Name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Name is required.",
            negated: true,
        },
        isIn: {
            options: [["FRIDGE", "WASHING_MACHINE", "COFFEE_MACHINE", "TV"]],
            errorMessage: "Invalid Name."
        },
        optional: true
    },
    "appliances.*.selected": {
        exists: {
            errorMessage: "Selected is missing.",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Selected is required.",
            negated: true,
        },
        optional: true
    },
    "comfort": {
        exists: {
            errorMessage: "Comfort is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Comfort is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "Comfort should be an object.",
        },
        optional: true
    },
    "comfort.floorLevel": {
        exists: {
            errorMessage: "FloorLevel is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "FloorLevel is required.",
            negated: true,
        },
        optional: true
    },
    "comfort.others": {
        exists: {
            errorMessage: "Others are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Others must be an array.",
        },
        isEmpty: {
            errorMessage: "Others are required",
            negated: true,
        },
        optional: true
    },
    "comfort.others.*.name": {
        exists: {
            errorMessage: "Name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Name is required.",
            negated: true,
        },
        isIn: {
            options: [["LIFT", "TERRACE", "BALCONY"]],
            errorMessage: "Invalid Name."
        },
        optional: true
    },
    "comfort.others.*.selected": {
        exists: {
            errorMessage: "Selected is missing.",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Selected is required.",
            negated: true,
        },
        optional: true
    },
    "features": {
        exists: {
            errorMessage: "Features are missing",
        },
        in: ["body"],
        isArray: {
            errorMessage: "Features must be an array.",
        },
        isEmpty: {
            errorMessage: "Features are required",
            negated: true,
        },
        optional: true
    },
    "features.*.name": {
        exists: {
            errorMessage: "Name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Name is required.",
            negated: true,
        },
        isIn: {
            options: [["PARKING"]],
            errorMessage: "Invalid Name."
        },
        optional: true
    },
    "features.*.selected": {
        exists: {
            errorMessage: "Selected is missing.",
        },
        in: ["body"],
        isBoolean: {
            errorMessage: "Selected must be a boolean.",
        },
        isEmpty: {
            errorMessage: "Selected is required.",
            negated: true,
        },
        optional: true
    },
});






