import { checkSchema } from "express-validator";
export const approveOrRejectLeaseApplicationSchema = checkSchema({
    "status": {
        exists: {
            errorMessage: "status is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "status is required.",
            negated: true,
        },
        isIn: {
            options: [["APPROVED", "DENIED"]],
            errorMessage: "Invalid Status."
        },
    }
});