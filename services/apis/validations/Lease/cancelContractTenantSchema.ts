import { checkSchema } from "express-validator";
export const cancelContractTenantSchema = checkSchema({
    "note": {
        exists: {
            errorMessage: "Notes is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Notes is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Notes should be a string.",
        },
    },
    "startAsSoonAsPossible": {
        exists: {
            errorMessage: "startAsSoonAsPossible is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "startAsSoonAsPossible is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "startAsSoonAsPossible should be a boolean.",
        },
    },
    "moveOutDate": {
        exists: {
            errorMessage: "moveOutDate is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "moveOutDate is required.",
            negated: true,
        },
        isDate: {
            errorMessage: "moveOutDate should be a string.",
        },
    },
    "amount": {
        exists: {
            errorMessage: "amount is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "amount is required.",
            negated: true,
        },
        isFloat: {
            errorMessage: "amount should be a number.",
        },
    },
    "paymentReference": {
        exists: {
            errorMessage: "paymentReference is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "paymentReference is required.",
            negated: true,
        },
        isString: {
            errorMessage: "paymentReference should be a string.",
        },
    }
});