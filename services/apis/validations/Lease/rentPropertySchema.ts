import { checkSchema } from "express-validator";
export const rentPropertySchema = checkSchema({
    "years": {
        exists: {
            errorMessage: "Year is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Year is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Year should be an integer.",
        },
    },
    "months": {
        exists: {
            errorMessage: "Month is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Month is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Month should be an integer.",
        },
    },
    "startAsSoonAsPossible": {
        exists: {
            errorMessage: "StartAsSoonAsPossible is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "StartAsSoonAsPossible is required.",
            negated: true,
        },
        isBoolean: {
            errorMessage: "StartAsSoonAsPossible should be a boolean.",
        },
    },
    "customStartDate": {
        exists: {
            errorMessage: "CustomStartDate is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "CustomStartDate is required.",
            negated: true,
        },
        isDate: {
            errorMessage: "CustomStartDate should be a date.",
        },
    },
    "payPerMonth": {
        exists: {
            errorMessage: "payPerMonth is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "payPerMonth is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "payPerMonth should be an integer.",
        },
    },
    "paidInAdvance": {
        exists: {
            errorMessage: "PaidInAdvance is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PaidInAdvance is required.",
            negated: true,
        },
        isIn: {
            options: [["1 month","2 months","3 months"]],
            errorMessage: "Invalid paid in advance value."
        },
    },
    "firstRentPaidDue": {
        exists: {
            errorMessage: "firstRentPaidDue is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "firstRentPaidDue is required.",
            negated: true,
        },
        isDate: {
            errorMessage: "firstRentPaidDue should be a date.",
        },
    },
    "dueDateNotificationPeriod": {
        exists: {
            errorMessage: "DueDateNotificationPeriod is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "DueDateNotificationPeriod is required.",
            negated: true,
        },
        isIn: {
            options: [["3 days before","7 days before", "10 days before","14 days before"]],
            errorMessage: "Invalid DueDateNotificationPeriod."
        },
    },
    "deposit": {
        exists: {
            errorMessage: "Deposit is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Deposit is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Deposit should be an integer.",
        },
    },
    "lateRentFee": {
        exists: {
            errorMessage: "LateRentFee is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "LateRentFee is required.",
            negated: true,
        },
        isInt: {
            errorMessage: "Deposit should be an integer.",
        },
    },
    "inspectionInterval": {
        exists: {
            errorMessage: "InspectionInterval is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "InspectionInterval is required.",
            negated: true,
        },
        isIn: {
            options: [["Every 3 months","Every 6 months", "Every 1 year"]],
            errorMessage: "Invalid InspectionInterval."
        },
    },
    "overDueRentNotifyPeriod": {
        exists: {
            errorMessage: "overDueRentNotifyPeriod is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "overDueRentNotifyPeriod is required.",
            negated: true,
        },
        isIn: {
            options: [["Everyday","Every alternate day"]],
            errorMessage: "Invalid overDueRentNotifyPeriod."
        },
    },
    "paymentAccount": {
        exists: {
            errorMessage: "PaymentAccount is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PaymentAccount is required.",
            negated: true,
        },
        isObject: {
            errorMessage: "PaymentAccount should be an object.",
        },
    },
    "paymentAccount.accountNumber": {
        exists: {
            errorMessage: "PaymentAccount accountNumber is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PaymentAccount accountNumber is required.",
            negated: true,
        },
        isString: {
            errorMessage: "AccountNumber should be a string.",
        },
    },
    "paymentAccount.name": {
        exists: {
            errorMessage: "PaymentAccount name is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PaymentAccount name is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Name should be a string.",
        },
    },
    "paymentAccount.branch": {
        exists: {
            errorMessage: "PaymentAccount branch is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PaymentAccount branch is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Branch should be a string.",
        },
    },
    "leaseExpiryNotificationPeriod": {
        exists: {
            errorMessage: "LeaseExpiryNotificationPeriod is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "LeaseExpiryNotificationPeriod is required.",
            negated: true,
        },
        isIn: {
            options: [["30 days","60 days","90 days"]],
            errorMessage: "Invalid LeaseExpiryNotificationPeriod."
        },
    },
    "propertyManager": {
        exists: {
            errorMessage: "PropertyManager is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "PropertyManager is required.",
            negated: true,
        },
        isString: {
            errorMessage: "PropertyManager should be a string.",
        },
    },
    "notes": {
        exists: {
            errorMessage: "Notes is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Notes is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Notes should be a string.",
        },
    },
    "tenant": {
        exists: {
            errorMessage: "Tenant is missing.",
        },
        in: ["body"],
        isEmpty: {
            errorMessage: "Tenant is required.",
            negated: true,
        },
        isString: {
            errorMessage: "Tenant should be a string.",
        },
    },
})