export interface IPublishProperty {
    status: string,
    availableSince: string,
    rentPerMonth: number,
    deposite: number,
    applicationFee: number,
    propertyManager: string,
    requirements: {
        showMonthlyIncome: boolean,
        showDocumentWIthPicture: boolean,
        showIncomeProof: boolean,
        fixInsurance: boolean
    }
}