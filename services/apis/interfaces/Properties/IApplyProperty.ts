export interface IApplyProperty {
    years: number,
    months: number,
    totalResidents: number,
    startAsSoonAsPossible: boolean,
    customStartDate: string,
    notes: string,
    verifyMeBy: boolean,
    securityNumber: {
        documentType: string,
        number: string
    },
    currentLandlord: {
        propertyTenantedEarlier: string,
        name: string,
        address: string,
        phone: string,
        ownerEmail: string,
        monthlyRent: string,
        tenantEmail: string
    },
    employer: {
        employerId: string,
        companyName: string,
        companyAddress: string,
        supervisorName: string,
        supervisorEmail: string,
        monthlyIncome: string,
    },
    salarySlips: { 
        salarySlipOfLatestMonth: string,
        salarySlipOfPreviousMonth: string
    }
}