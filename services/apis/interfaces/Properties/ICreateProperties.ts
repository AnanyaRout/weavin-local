export interface ICreateProperty {
    status: string,
    buildingType: string,
    address: {
        houseNumber: string,
        street: string,
        locality: string,
        country: string,
        long: number,
        lat: number
    },
    contact: string,
    title: string,
    description: string,
    images: [{
        title: string,
        mainImage: boolean
    }],
    size: {
        surface: number,
        bedRooms: number,
        bathRooms: number
    },
    utilities: [{
        name: string,
        selected: boolean
    }],
    amenities: [{
        name: string,
        selected: boolean
    }],
    appliances: [{
        name: string,
        selected: boolean
    }],
    comfort: {
        floorLevel: string,
        others: [{
            name: string,
            selected: boolean
        }]
    },
    features: [{
        name: string,
        selected: boolean
    }],

}