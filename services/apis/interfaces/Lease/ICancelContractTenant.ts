export interface ICancelContractTenant {
    note: string,
    startAsSoonAsPossible: boolean,
    moveOutDate: string,
    amount: string,
    paymentReference: string
}