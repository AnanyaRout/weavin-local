export interface IRentProperty {
    years: number,
    months: number,
    startAsSoonAsPossible: boolean,
    customStartDate: string,
    payPerMonth: number,
    paidInAdvance: string,
    firstRentPaidDue: string,
    dueDateNotificationPeriod: string,
    deposit: number,
    lateRentFee: number,
    inspectionInterval: string,
    overDueRentNotifyPeriod: string,
    paymentAccount: {
        accountNumber: string,
        name: string,
        branch: string,
    },
    leaseExpiryNotificationPeriod: string,
    propertyManager: string
    notes: string,
    tenant: string
}