
import { dynamoDbClient } from "../../dbConnection";

exports.handler = async (event, _context, callback) => {
    const datas = {
        uuid: event.request.userAttributes.sub,
        user_email: event.request.userAttributes.email,
        user_name: event.userName
    };
    if (event.request.userAttributes.email) {
        const user_created_date = new Date().toString();

        const param = {
            TableName: "Users",
            Item: {
                PK: "user_" + datas.uuid,
                SK: "user_email#" + datas.user_email + "user_name#" + datas.user_name + "user_created_date#" + user_created_date,
                user_email: datas.user_email,
                user_name: datas.user_name,
                user_created_date
            },
        };
        await dynamoDbClient.put(param).promise();
        // Return to Amazon Cognito
        callback(null, event);
    } else {
        // Nothing to do, the user's email ID is unknown
        callback(null, event);
    }
};

