"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getMe } from "../../apis/controllers/User/getMe";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getMe,
    // schema: getMeSchema,
    data: event
});
