"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getPropertyImage } from "../../apis/controllers/Properties/getPropertyImage";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getPropertyImage,
    data: event
});
