"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { editProperty } from "../../apis/controllers/Properties/editProperty";
import { createPropertySchema } from "../../apis/validations/Properties/createPropertySchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: editProperty,
    schema: createPropertySchema,
    data: event
});
