"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getOneProperty } from "../../apis/controllers/Properties/getOneProperty";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getOneProperty,
    data: event
});
