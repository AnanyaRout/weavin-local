"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getAllProperties } from "../../apis/controllers/Properties/getAllProperties";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getAllProperties,
    data: event
});
