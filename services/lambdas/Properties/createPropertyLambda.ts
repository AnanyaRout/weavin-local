"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { createProperty } from "../../apis/controllers/Properties/createProperty";
import { createPropertySchema } from "../../apis/validations/Properties/createPropertySchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
        controller: createProperty,
        schema: createPropertySchema,
        data: event
    });
