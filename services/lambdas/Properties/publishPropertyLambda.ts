"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { publishProperty } from "../../apis/controllers/Properties/publishProperty";
import { publishPropertySchema } from "../../apis/validations/Properties/publishPropertySchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: publishProperty,
    schema: publishPropertySchema,
    data: event
});
