"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { searchProperties } from "../../apis/controllers/Properties/searchProperty";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: searchProperties,
    data: event
});
