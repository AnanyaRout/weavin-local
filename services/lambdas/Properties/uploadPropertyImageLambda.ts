"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { uploadPropertyImages } from "../../apis/controllers/Properties/uploadPropertyImages";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: uploadPropertyImages,
    data: event
});
