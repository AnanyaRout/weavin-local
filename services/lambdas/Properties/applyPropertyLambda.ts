"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { applyProperty } from "../../apis/controllers/Properties/applyProperty";
import { applyPropertySchema } from "../../apis/validations/Properties/applyPropertySchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: applyProperty,
    schema: applyPropertySchema,
    data: event
});
