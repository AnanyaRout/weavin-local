"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { viewLeaseApplication } from "../../apis/controllers/Lease/viewLeaseApplication";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: viewLeaseApplication,
    data: event
});
