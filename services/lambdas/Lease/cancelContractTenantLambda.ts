"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { cancelContractTenant } from "../../apis/controllers/Lease/cancelContractTenant";
import { cancelContractTenantSchema } from "../../apis/validations/Lease/cancelContractTenantSchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: cancelContractTenant,
    schema: cancelContractTenantSchema,
    data: event
});
