"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { contractSignTenant } from "../../apis/controllers/Lease/signTenantContract";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: contractSignTenant,
    data: event
});
