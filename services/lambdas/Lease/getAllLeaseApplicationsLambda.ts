"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getAllLeaseApplications } from "../../apis/controllers/Lease/getAllLeaseApplications";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getAllLeaseApplications,
    data: event
});
