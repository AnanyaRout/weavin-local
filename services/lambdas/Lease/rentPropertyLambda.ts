"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { rentProperty } from "../../apis/controllers/Lease/rentProperty";
import { rentPropertySchema } from "../../apis/validations/Lease/rentPropertySchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: rentProperty,
    data: event,
    schema: rentPropertySchema
});
