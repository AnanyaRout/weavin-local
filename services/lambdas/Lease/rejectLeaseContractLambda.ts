"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { rejectLeaseContract } from "../../apis/controllers/Lease/rejectLeaseContract";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: rejectLeaseContract,
    data: event
});
