"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { approveOrRejectLeaseApplication } from "../../apis/controllers/Lease/approveOrRejectLeaseAppication";
import { approveOrRejectLeaseApplicationSchema } from "../../apis/validations/Lease/approveOrRejectLeaseApplicationSchema";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: approveOrRejectLeaseApplication,
    schema: approveOrRejectLeaseApplicationSchema,
    data: event
});
