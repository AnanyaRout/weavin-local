"use strict";
import { APIGatewayProxyEvent } from "aws-lambda";
import { controllerHandler } from "../../../shared/lib/controllerHandler";
import { getAllTenantProperty } from "../../apis/controllers/Lease/getAllTenantProperty";
export const handler = async (event: APIGatewayProxyEvent) => controllerHandler({
    controller: getAllTenantProperty,
    data: event
});
