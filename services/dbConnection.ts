const AWS = require("aws-sdk");
let options= {};
if(process.env.STAGE=== "local") {
  options= {
    region: "localhost",
    endpoint: "http://localhost:8000",
    maxRetries: 1
  };
}


export const dynamoDbClient = new AWS.DynamoDB.DocumentClient(options);
