
import { IControllerHandlerParams } from "../interfaces/IControllerHandlerParams";
import { handleError } from "./handleError";
import { Created, Ok } from "./response";
import { validator } from "./validator/validator";

/**
 * Controller handler method which validates data with defined schema, call controller and
 * handles the exception.
 * @param param
 */
export const controllerHandler = async ({ schema, controller, data }: IControllerHandlerParams) => {

    const req = data;
    req.body = JSON.parse(req.body);
    try {
        const user: any = {};
        if ((data.requestContext&& data.requestContext.authorizer)) {
            user.uuid = "user_" +data.requestContext.authorizer.claims.sub;
            user.email = data.requestContext.authorizer.claims.email;
            // user.role = req.user.claims["custom:role"];
        }

        let payload = {};
        payload = { ...(req.body ? req.body : {}), ...req.pathParameters, ...(req.queryStringParameters ? req.queryStringParameters : {}) };


        // Call the controller method
        const params = {
            args: {
                params: req.pathParameters ? req.pathParameters : {},
                queryString: req.queryStringParameters ? req.queryStringParameters : {},
            },
            input: payload,
            user,
            req
        };
        // Validate the user input
        if (schema) {
            const errors = await validator(schema, {
                body: payload,
                query: params.args.queryString,
                path: params.args.params,
            });
            if (errors) {
                return errors;
            }
        }
        const response = await controller(params);


        // Return response to the client
        const method = req.httpMethod === "POST" && response.created ? Created : Ok;
        return method(response.message, response.payload);

    } catch (e) {
        console.log("caught");
        console.log(e);
        const data = handleError(e);
        console.log(".....error...", data);
        return data;
    }
};
