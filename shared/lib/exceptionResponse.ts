
export function NotFound() {
    return HttpStatus(404);
}

/**
 * Un authorized Response
 * @param res
 */
export function UnAuthorized() {
    return HttpStatus( 401);
}

/**
 * Bad Request
 * @param res
 * @param message
 */
export function BadRequest(message: string = "BAD_REQUEST") {
    return HttpStatus( 400, {message});
}

/**
 * Un Processable entity Response
 * @param res
 * @param errors
 */
export function UnProcessableEntity( errors: any) {
    return HttpStatus(422, errors);
}

/**
 * Created Response
 * @param res
 * @param data
 */
export function Created( data: any) {
    return HttpStatus( 201, data);
}

/**
 * Ok Response
 * @param res
 * @param data
 */
export function Ok(data: any = {}) {
    return HttpStatus( 200, data);
}

/**
 * No Content Response
 * @param res
 */
export function NoContent() {
    return HttpStatus(204);
}

/**
 * Forbidden Response
 * @param res
 */
export function Forbidden(info?) {
    return HttpStatus( 403, info);
}

/**
 * Internal server error Response
 * @param res
 */
export function InternalServerError( err?) {
    if (err) {
        // tslint:disable-next-line: no-console
        console.log("ISE", err);
    }
    return HttpStatus( 500);
}

/**
 * HTTP Status
 * @param res
 * @param code
 * @param info
 */
export function HttpStatus( code: number = 200, info: any = {}) {
    return {
        statusCode: code,
        body: JSON.stringify(info)
    };
}
