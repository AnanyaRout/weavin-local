import { BadRequest, InternalServerError, NotFound, UnprocessedEntity, NoContent } from "./response";

export const handleError = (error: any ) => {
    if (error.name === "HttpNotFound") {
        return NotFound( error.message);
    }

    if (error.name === "HttpBadRequest") {
        return BadRequest(error.message);
    }

    if (error.name === "HttpNoContent") {
        return NoContent( error.message);
    }

    if (error.name === "ValidationError") {
        return UnprocessedEntity("Unprocessed entity", error.errors);
    }
    return InternalServerError("Internal Server Error");
};
