import {
    BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR,
    NOT_FOUND, OK, UNPROCESSED_ENTITY, NO_CONTENT
} from "../constants/httpStatusCodes";
import { isArrayNotEmpty, isObject } from "../helpers/validChecker";
import { IResponsePayload } from "../interfaces/IResponsePayload";

export const Created = (message: string, payload: IResponsePayload) => {
    return respond(CREATED, message, payload);
};

export const Ok = (message: string, payload: IResponsePayload | IResponsePayload[]) => {
    return respond(OK, message, payload);
};

export const NotFound = (message: string) => {
    return respond(NOT_FOUND, message);
};

export const UnprocessedEntity = ( message: string, payload: IResponsePayload) => {
    return respond(UNPROCESSED_ENTITY, message, payload);
};

export const BadRequest = (message: string) => {
    return respond(BAD_REQUEST, message);
};

export const NoContent = (message: string) => {
    return respond(NO_CONTENT, message);
};

export const InternalServerError = (message: string) => {
    return respond(INTERNAL_SERVER_ERROR, message);
};

const respond = (code: number, message: string, payload?: IResponsePayload | IResponsePayload[]) => {
    const responseInfo: { message: string, payload?: IResponsePayload } = { message };

    if (isNonEmptyObject(payload) || Array.isArray(payload)) {
        responseInfo.payload = payload;
    }
    // return res.status(code).send(responseInfo);
    return {
        statusCode: code,
        body: JSON.stringify(responseInfo)
    }
};

const isNonEmptyObject = (value: any) => {
    return value && isObject(value) && isArrayNotEmpty(Object.keys(value));
};
