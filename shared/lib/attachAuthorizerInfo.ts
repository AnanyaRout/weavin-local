import { APIGatewayProxyEvent } from "aws-lambda";

export const attachAuthorizerInfo = (_event: APIGatewayProxyEvent) => {

  return (...parameters: any[]) => {


    if (parameters[0].requestContext.authorizer) {
      parameters[0].user = parameters[0].requestContext.authorizer;
    }
    parameters[2]();
  };
};
