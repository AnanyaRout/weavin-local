export const randomPassword = () => {
    // const length=8;
    const alphaChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const  smallChar= "abcdefghijklmnopqrstuvwxyz";
    const specialChar = "@#$";
    const number= "1234567890"

    let pass = "";
    for (let x = 0; x < 2; x++) {
        const i = Math.floor(Math.random() * alphaChar.length);
        pass += alphaChar.charAt(i);
    }
    for (let x = 0; x < 2; x++) {
        const i = Math.floor(Math.random() * smallChar.length);
        pass += smallChar.charAt(i);
    }
    for (let x = 0; x < 1; x++) {
        const i = Math.floor(Math.random() * specialChar.length);
        pass += specialChar.charAt(i);
    }
    
    for (let x = 0; x < 3; x++) {
        const i = Math.floor(Math.random() * number.length);
        pass += number.charAt(i);
    }
    return pass;
  };
  