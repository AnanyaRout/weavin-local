import { createHash } from "crypto";

/**
 * generate a salted sha256 string.
 * @param key
 * @param salt
 */
export const generateSha256Password = (key: string, salt?: string): string => {
    const secret: string = salt || process.env.SHA256_PASSWORD_SALT;
    return createHash("sha256").update(key + secret).digest("hex");
};
