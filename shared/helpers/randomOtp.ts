export const randomOtp = () => {
  const length=6;
  const chars = "1234567890";
  let pass = "";
  for (let x = 0; x < length; x++) {
      const i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
  }
  return pass;
};
