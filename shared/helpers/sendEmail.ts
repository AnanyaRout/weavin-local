import { Credentials, SES } from "aws-sdk";
import * as nodemailer from "nodemailer";
import "reflect-metadata";
import "source-map-support/register";
import { HttpBadRequest } from "../lib/exceptions/HttpBadRequest";
import { template } from "lodash";
import { dynamoDbClient } from "../../services/dbConnection";

/**
 * The handler method which the aws api gateway gets executed.
 * @param event
 * @param context
 */
export const sendEmail = async (to: string | string[], identifier: string, info: IEmailBody) => {

    const transporter = await nodemailer.createTransport({
        SES: new SES({
            apiVersion: "latest",
            credentials: new Credentials({
                accessKeyId: process.env.ACCESS_KEY,
                secretAccessKey: process.env.ACCESS_SECRET,
            }),
            region: process.env.AWS_REGION_NAME,
        }),
    });


    const templates= await dynamoDbClient.query({
        TableName: "Users",
        KeyConditionExpression: "PK= :PK AND SK=:SK",
        ExpressionAttributeValues: {
            ":PK": "Email_Template",
            ":SK": "Email_Template"
        },
    }).promise();


    let templateSource;
    templates.Items[0].data.map((each)=> {
        if(each.identifier === identifier) {
            templateSource= each
        }
    }); 

    if (!templateSource) {
        throw new HttpBadRequest("Template not found.")
    }
    // send mail with defined transport object
    let information;
    try {
        information = await transporter.sendMail({
            from: process.env.SES_EMAIL_FROM, // sender address
            to, // list of receivers
            html: template(templateSource.template)(info.body),
            subject: template(templateSource.subject)(info.subject),
        });
    } catch (e) {
        // tslint:disable-next-line: no-console
        console.log("...........................error.......................",e);
        throw new HttpBadRequest(e);
    }

    // tslint:disable-next-line: no-console
    console.log("Message sent: %s", information.messageId);
    // tslint:disable-next-line: no-console
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(information));

    // tslint:disable-next-line: no-console
    console.log("EMAIL HAS BEEN SENT");
};


export interface IEmailBody {
    body: any;
    subject: any;
}