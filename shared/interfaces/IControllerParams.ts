import { IDynamicObject } from "./IDynamicObject";
// import { Request } from "express";

export interface IControllerParams<T> {
  args: {
    params: IDynamicObject,
    queryString: IDynamicObject,
  };
  input?: T;
  // req: Request;
  user: IAuthorizedUser;
}

export interface IAuthorizedUser {
  uuid: string;
  role: string;
}
