import { ERequestPayLoadTypes } from "../enums/RequestPayLoadTypes";

export interface IControllerHandlerParams {
    schema?: any;
    // tslint:disable-next-line: ban-types
    controller: Function;
    data?: any,
    schemaLookup?: ERequestPayLoadTypes;
    options?: any;
    // res: any
}
