export const SIGN_UP_SUCCESSFUL = "Sign up was successful.";
export const ACCOUNT_VERIFIED = "Email id validated successfully.";
export const LOGIN_SUCCESSFUL = "Login Successful.";
export const ENTITY_IN_SELLER_DETAILS_UPDATED_SUCCESSFULLY = "Entity in seller details updated successfully.";
export const SELLER_DETAILS_UPDATED_SUCCESSFULLY = "Seller details updated successfully.";
export const DOCUMENT_UPLOADED_SUCCESSFUL = "Document uploaded successfully.";
export const SUCCESSFUL = "Successful.";
export const REJECTED_SUCCESSFULLY = "Rejected successfully.";
export const APPROVED_SUCCESSFULLY = "Approved successfully.";
export const ANALYST_CREATED_SUCCESSFULLY = "Analyst created successfully.";
export const ANALYST_UPDATED_SUCCESSFULLY = "Analyst updated successfully.";
export const ANALYST_DELETED_SUCCESSFULLY = "Analyst deleted successfully.";
export const RESET_PASSWORD_SUCCESSFULLY = "Password reset successful."
export const BUYER_DETAILS_UPDATED_SUCCESSFULLY = "Buyer details updated successfully.";
export const FORGET_PASSWORD_MESSAGE = "Please check your email for instructions";