import { HttpBadRequest } from "../lib/exceptions/HttpBadRequest";

const AWS = require('aws-sdk')
AWS.config.update({ region: "ap-south-1" })
const ddb = new AWS.DynamoDB();
const ddbGeo = require('dynamodb-geo')
const config = new ddbGeo.GeoDataManagerConfiguration(ddb, 'Property_Location')
const myGeoTableManager = new ddbGeo.GeoDataManager(config);

export const searchPropertyTableSetup = async () => {
    // Pick a hashKeyLength appropriate to your usage
    config.hashKeyLength = 5


    // Use GeoTableUtil to help construct a CreateTableInput.
    const createTableInput = ddbGeo.GeoTableUtil.getCreateTableRequest(config)

    // Tweak the schema as desired
    createTableInput.ProvisionedThroughput.ReadCapacityUnits = 5

    console.log('Creating table with schema:')
    console.dir(createTableInput, { depth: null })

    // Create the table
    ddb.createTable(createTableInput).promise()
        // Wait for it to become ready
        .then(function () { return ddb.waitFor('tableExists', { TableName: config.tableName }).promise() })
        .then(function () { console.log('Table created and ready!') })
}


export const updateSearchPropertyTable = async (data) => {
    await myGeoTableManager.putPoint({
        RangeKeyValue: { S: data.id }, // Use this to ensure uniqueness of the hash/range pairs.
        GeoPoint: { // An object specifying latitutde and longitude as plain numbers. Used to build the geohash, the hashkey and geojson data
            latitude: data.lat,
            longitude: data.long
        },
        PutItemInput: { // Passed through to the underlying DynamoDB.putItem request. TableName is filled in for you.
            Item: { // The primary key, geohash and geojson data is filled in for you
                propertyId: { S: "property_" + data.id },
            },
        }
    }).promise()
        .then(function () { console.log('Done!') });
}

export const editSearchPropertyTable = async (data) => {
    await myGeoTableManager.updatePoint({
        RangeKeyValue: { S: data.id },
        GeoPoint: { // An object specifying latitutde and longitude as plain numbers.
            latitude: data.lat,
            longitude: data.long
        },
        UpdateItemInput: { // TableName and Key are filled in for you
            UpdateExpression: 'SET propertyId = :propertyId',
            ExpressionAttributeValues: {
                ':propertyId': { S: "property_" + data.id }
            }
        }
    }).promise()
        .then(function () { console.log('Done!') });
}
export const searchProperty = async (data) => {
    try {
        const res = await myGeoTableManager.queryRadius({
            RadiusInMeter: data.radius,
            CenterPoint: {
                latitude: data.lat,
                longitude: data.long
            }
        });

        return res;
    } catch (e) {
        console.log(".e.",e);
        throw new HttpBadRequest("Give some valid radius, lat and long value");
    }

}

export const deleteProperty = async (data) => {
    await myGeoTableManager.deletePoint({
        RangeKeyValue: { S: data.id },
        GeoPoint: { // An object specifying latitutde and longitude as plain numbers.
            latitude: data.lat,
            longitude: data.long
        },
        DeleteItemInput: { // Optional, any additional parameters to pass through.
            // TableName and Key are filled in for you
            // Example: Only delete if the point does not have a country name set
            ConditionExpression: 'propertyId = :propertyId',
            ExpressionAttributeValues: {
                ':propertyId': { S: "property_" + data.id }
            }
        }
    }).promise()
        .then(function () { console.log('Done!') });

}