export const fileActions = async ( key: string, action: string) => {

  const S3 = require('aws-sdk/clients/s3');
  const s3 = new S3();

  const params = {
    Bucket: process.env.BUCKET_NAME,
    Key: key,
    Expires: 60 * 5
  };
  const presignedURL = await s3.getSignedUrl(action, params);
  console.log(presignedURL);


  return presignedURL;
}