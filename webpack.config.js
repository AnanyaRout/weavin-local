const slsw = require("serverless-webpack");
const path = require("path");

module.exports = {
  devtool: "source-map",
  entry: slsw.lib.entries,
  mode: slsw.lib.webpack.isLocal ? "development" : "production",
  externals: ['pg', 'sqlite3', 'pg-hstore'],
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader", exclude: __dirname + "public" ,
      exclude: /node_modules\/(?!(pg|sqlite3|pg-hstore|pg-native|node-pre-gyp|postgres)\/).*/
    },],
  },
  optimization: {
    minimize: false,
  },
  output: {
    filename: "[name].js",
    libraryTarget: "commonjs",
  },
  resolve: {
    extensions: [".js", ".json", ".ts"],
  },
  target: "node",
};
