export interface ITokenData {
    id: string;
    role: string;
}
