import { AuthorizationData } from "aws-sdk/clients/ecr";
import "source-map-support/register";
import { Access } from "./enums/Access";
import { generatePolicy } from "./generatePolicy";
import { ITokenData } from "./interfaces/ITokenData";
import { verifyToken } from "./verifyToken";

/**
 * The method renders a template from the given parameters and send it to multiple users.
 * @param event
 */
export const handler = async (event: AuthorizationData & { methodArn: string }) => {
  console.log("Event", event);
  console.log(event);
  const token = event.authorizationToken;
  let info: ITokenData;
  try {
    info = await verifyToken(token.slice(7, token.length));
    console.log("Success", info);
  } catch (e) {
    throw new Error("Unauthorized");
  }

  return generatePolicy(info.id, Access.Allow, "*", info);
};
